/*
 *************************************************************************
 *                                                                       *
 * Vega FEM Simulation Library Version 2.2                               *
 *                                                                       *
 * "volumetricMesh" library , Copyright (C) 2007 CMU, 2009 MIT, 2015 USC *
 * All rights reserved.                                                  *
 *                                                                       *
 * Code author: Jernej Barbic                                            *
 * http://www.jernejbarbic.com/code                                      *
 *                                                                       *
 * Research: Jernej Barbic, Fun Shing Sin, Daniel Schroeder,             *
 *           Doug L. James, Jovan Popovic                                *
 *                                                                       *
 * Funding: National Science Foundation, Link Foundation,                *
 *          Singapore-MIT GAMBIT Game Lab,                               *
 *          Zumberge Research and Innovation Fund at USC                 *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of the BSD-style license that is            *
 * included with this library in the file LICENSE.txt                    *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the file     *
 * LICENSE.TXT for more details.                                         *
 *                                                                       *
 ************************************************************************
*/
/*
 * tetMesh.h Modified
 *
 *  Created on: Mar 24, 2016
 *      Author: santidan
 */

/*
  This class is a container for a tetrahedral volumetric 3D mesh. See
  also volumetricMesh.h. The tetrahedra can take arbitrary shapes (not
  limited to only a few shapes).
*/


#ifndef TETMESH_H_
#define TETMESH_H_

#include "volumetricMesh.h"

// see also volumetricMesh.h for a description of the routines

class TetMesh: public VolumetricMesh
{
public:
	// loads the mesh from a file
	// ASCII: .veg text input formut, see documentation and the provided examples
	// BINARY: .vegb binary input format
	TetMesh(const char * filename, fileFormatType fileFormat = ASCII,
			int verbose = 1);

	TetMesh(const TetMesh & tetMesh);
	virtual VolumetricMesh * clone();
	virtual ~TetMesh();

// === misc queries ===
	static VolumetricMesh::elementType elementType()
	{
		return elementType_;
	}
	virtual VolumetricMesh::elementType getElementType() const
	{
		return elementType();
	}
	static double getTetVolume(Vec3d * a, Vec3d * b, Vec3d * c, Vec3d * d);
	static double getTetDeterminant(Vec3d * a, Vec3d * b, Vec3d * c, Vec3d * d);
	virtual double getElementVolume(int el) const;
	virtual void computeBarycentricWeights(int el, Vec3d pos,
			double * weights) const;
	// Added by Santidan 03-April-2015
	// calculating gradL1, gradL2, gradL3, gradL4
	Vec3d ** computeGradL(Vec3d * a, Vec3d * b, Vec3d * c, Vec3d * d);

protected:
	static const VolumetricMesh::elementType elementType_;

};

#endif /* TETMESH_H_ */
