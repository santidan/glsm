/*
 * ************************************************************************
 *                                                                       *
 * Vega FEM Simulation Library Version 2.2                               *
 *                                                                       *
 * "volumetricMesh" library , Copyright (C) 2007 CMU, 2009 MIT, 2015 USC *
 * All rights reserved.                                                  *
 *                                                                       *
 * Code authors: Jernej Barbic, Yijing Li                                *
 * http://www.jernejbarbic.com/code                                      *
 *                                                                       *
 * Research: Jernej Barbic, Fun Shing Sin, Daniel Schroeder,             *
 *           Doug L. James, Jovan Popovic                                *
 *                                                                       *
 * Funding: National Science Foundation, Link Foundation,                *
 *          Singapore-MIT GAMBIT Game Lab,                               *
 *          Zumberge Research and Innovation Fund at USC                 *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of the BSD-style license that is            *
 * included with this library in the file LICENSE.txt                    *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the file     *
 * LICENSE.TXT for more details.                                         *
 *                                                                       *
 *************************************************************************
 * volumetricMesh.h Modified
 *
 *  Created on: Mar 24, 2016
 *      Author: santidan
 */

#ifndef VOLUMETRICMESH_H_
#define VOLUMETRICMESH_H_

/*
 This abstract class can store a generic volumetric 3D mesh.
 It stores the mesh geometric information (elements and vertices),
 and also the material parameters of each individual mesh element
 (Young's modulus, Poisson ratio, mass density). This is done by
 organizing elements with the same material parameters into a "region".
 The class supports several geometric queries and interpolation to
 an embedded triangle mesh ("Free-Form Deformation").  It also
 supports exporting the mesh to an .ele or .node format (the format
 used by the Stellar and TetGen mesh generation packages).  Derived classes
 are the TetMesh (general tetrahedral meshes), and CubicMesh
 (axis-aligned cubic "voxel" meshes). See description in tetMesh.h and cubicMesh.h.

 All quantities are 0-indexed, except the input mesh files where the
 elements and vertices are 1-indexed (same as in TetGen and Stellar).
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <set>
#include <string>
#include <map>
#include "minivector.h"

class VolumetricMesh
{
public:

	// Note: This class is abstract and cannot be instantiated; use the constructors in the derived classes (TetMesh, CubicMesh) to initialize a mesh, or use the load routine in volumetricMeshLoader.h

	// copy constructor, destructor
	VolumetricMesh(const VolumetricMesh & volumetricMesh);
	virtual VolumetricMesh * clone() = 0;
	virtual ~VolumetricMesh();
    typedef enum {INVALID, TET, CUBIC} elementType;
	typedef enum {ASCII, BINARY, NUM_FILE_FORMATS} fileFormatType; // ASCII is the text .veg format, BINARY is the binary .vegb format
	// opens the file and returns the element type of the volumetric mesh in the file; returns INVALID if no type information found
	static elementType getElementType(const char * filename,fileFormatType fileFormat = ASCII);
	virtual elementType getElementType() const = 0; // calls the derived class to identify itself


	inline int getNumVertices() const{return numVertices;}
	inline Vec3d * getVertex(int i) const{return vertices[i];}
	inline Vec3d * getVertex(int element, int vertex) const{return vertices[elements[element][vertex]];}
	inline int getVertexIndex(int element, int vertex) const{return elements[element][vertex];}
	inline Vec3d ** getVertices() const{return vertices;} // advanced, internal datastructure
	inline int getNumElements() const{return numElements;}
	inline int getNumElementVertices() const{return numElementVertices;}

protected:
	int numVertices;
	Vec3d ** vertices;

	int numElementVertices;
	int numElements;
	int ** elements;

	VolumetricMesh(const char * filename, fileFormatType fileFormat,
			int numElementVertices, elementType * elementType_, int verbose);

	void loadFromAscii(const char * filename, elementType * elementType_, int verbose=0);

	static elementType getElementTypeASCII(const char * filename);

	elementType temp; // auxiliary

	friend class VolumetricMeshLoader;
};
#endif /* VOLUMETRICMESH_H_ */
