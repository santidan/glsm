/*
 * volumetricMeshLoader.h
 *
 *  Created on: Mar 24, 2016
 *      Author: santidan
 */

#ifndef VOLUMETRICMESHLOADER_H_
#define VOLUMETRICMESHLOADER_H_

#include "volumetricMesh.h"

class VolumetricMeshLoader
{
public:
	// loads a volumetric mesh (ASCII (.veg format), or BINARY)
	static VolumetricMesh * load(const char * filename,
			VolumetricMesh::fileFormatType fileFormat = VolumetricMesh::ASCII,
			int verbose = 1);
};

#endif /* VOLUMETRICMESHLOADER_H_ */
