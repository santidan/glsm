/*
 *************************************************************************
 *                                                                       *
 * Vega FEM Simulation Library Version 2.2                               *
 *                                                                       *
 * "volumetricMesh" library , Copyright (C) 2007 CMU, 2009 MIT, 2015 USC *
 * All rights reserved.                                                  *
 *                                                                       *
 * Code author: Jernej Barbic                                            *
 * http://www.jernejbarbic.com/code                                      *
 *                                                                       *
 * Research: Jernej Barbic, Fun Shing Sin, Daniel Schroeder,             *
 *           Doug L. James, Jovan Popovic                                *
 *                                                                       *
 * Funding: National Science Foundation, Link Foundation,                *
 *          Singapore-MIT GAMBIT Game Lab,                               *
 *          Zumberge Research and Innovation Fund at USC                 *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of the BSD-style license that is            *
 * included with this library in the file LICENSE.txt                    *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the file     *
 * LICENSE.TXT for more details.                                         *
 *                                                                       *
 ************************************************************************
 */
/*
 * tetMesh.cpp
 *
 *  Created on: Mar 24, 2016
 *      Author: santidan
 */

#include "tetMesh.h"
#include <iostream>

const VolumetricMesh::elementType TetMesh::elementType_ = TET;

TetMesh::TetMesh(const char * filename, fileFormatType fileFormat, int verbose) :
		VolumetricMesh(filename, fileFormat, 4, &temp, verbose)
{
	if (temp != elementType_)
	{
		printf("Error: mesh is not a tet mesh.\n");
		throw 11;
	}
}


TetMesh::TetMesh(const TetMesh & source) :
		VolumetricMesh(source)
{
}

VolumetricMesh * TetMesh::clone()
{
	TetMesh * mesh = new TetMesh(*this);
	return mesh;
}

TetMesh::~TetMesh()
{
}

double TetMesh::getTetVolume(Vec3d * a, Vec3d * b, Vec3d * c, Vec3d * d)
{
	// volume = 1/6 * | (a-d) . ((b-d) x (c-d)) |
	// return (1.0 / 6 * fabs(dot(*a - *d, cross(*b - *d, *c - *d))));
	// santi-modified
	// volume = 1/6 * | (d-a) . ((b-a) x (c-a)) |
	return (1.0 / 6.0 * dot(*d - *a, cross(*b - *a, *c - *a)));
}

double TetMesh::getElementVolume(int el) const
{
	Vec3d * a = getVertex(el, 0);
	Vec3d * b = getVertex(el, 1);
	Vec3d * c = getVertex(el, 2);
	Vec3d * d = getVertex(el, 3);
	return getTetVolume(a, b, c, d);
}

//---------------------------------------
//Added by Santidan Date:04-April-2016
//---------------------------------------
Vec3d ** TetMesh::computeGradL( Vec3d * a, Vec3d * b, Vec3d * c,
		Vec3d * d)
{
	Vec3d vtx[4]=Vec3d(0.);
	Vec3d tmpGrad[4] =Vec3d(0.);
	vtx[0] = *a;
	vtx[1] = *b;
	vtx[2] = *c;
	vtx[3] = *d;
	double volume6 = 6.0 * getTetVolume(&vtx[0], &vtx[1], &vtx[2], &vtx[3]);
	//std::cout<<"volume6:   " <<volume6 << std::endl;
	//Vec3d d1, d2;
	for (int i = 0; i < 4; i++)
	{
		//std::cout << volume6 << std::endl;
		Vec3d d1 = vtx[(i + 2) % 4] - vtx[(i + 1) % 4];
		//  std::cout << d1 << std::endl;
		Vec3d d2 = vtx[(i + 3) % 4] - vtx[(i + 2) % 4];
		//  std::cout << d2 << std::endl;
		tmpGrad[i] = pow(-1, i + 1) * cross(d1, d2) / volume6;
		//  std::cout << tmpGrad[i] << std::endl;
	}

	Vec3d ** grads;
	grads = (Vec3d**) malloc(sizeof(Vec3d*) * 4);
	for (int i = 0; i < 4; i++)
	{
//	       grads[i] = new Vec3d(x,y,z);
		grads[i] = new Vec3d(tmpGrad[i]);
	    //std::cout << *grads[i] << std::endl;
	}

	return grads;

}

void TetMesh::computeBarycentricWeights(int el, Vec3d pos,
		double * weights) const
{
/*
       |x1 y1 z1 1|
  D0 = |x2 y2 z2 1|
       |x3 y3 z3 1|
       |x4 y4 z4 1|

       |x  y  z  1|
  D1 = |x2 y2 z2 1|
       |x3 y3 z3 1|
       |x4 y4 z4 1|

       |x1 y1 z1 1|
  D2 = |x  y  z  1|
       |x3 y3 z3 1|
       |x4 y4 z4 1|

       |x1 y1 z1 1|
  D3 = |x2 y2 z2 1|
       |x  y  z  1|
       |x4 y4 z4 1|

       |x1 y1 z1 1|
  D4 = |x2 y2 z2 1|
       |x3 y3 z3 1|

       |x  y  z  1|

  wi = Di / D0
*/

	Vec3d vtx[4];
	for (int i = 0; i < 4; i++)
		vtx[i] = *getVertex(el, i);

	double D[5];
	D[0] = getTetDeterminant(&vtx[0], &vtx[1], &vtx[2], &vtx[3]);

	for (int i = 1; i <= 4; i++)
	{
		Vec3d buf[4];
		for (int j = 0; j < 4; j++)
			buf[j] = vtx[j];
		buf[i - 1] = pos;
		D[i] = getTetDeterminant(&buf[0], &buf[1], &buf[2], &buf[3]);
		weights[i - 1] = D[i] / D[0];
	}
}

double TetMesh::getTetDeterminant(Vec3d * a, Vec3d * b, Vec3d * c, Vec3d * d)
{
	// computes the determinant of the 4x4 matrix
	// [ a 1 ]
	// [ b 1 ]
	// [ c 1 ]
	// [ d 1 ]

	Mat3d m0 = Mat3d(*b, *c, *d);
	Mat3d m1 = Mat3d(*a, *c, *d);
	Mat3d m2 = Mat3d(*a, *b, *d);
	Mat3d m3 = Mat3d(*a, *b, *c);

	return (-det(m0) + det(m1) - det(m2) + det(m3));
}

