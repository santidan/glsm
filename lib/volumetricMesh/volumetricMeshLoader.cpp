/*************************************************************************
 *                                                                       *
 * Vega FEM Simulation Library Version 2.2                               *
 *                                                                       *
 * "volumetricMesh" library , Copyright (C) 2007 CMU, 2009 MIT, 2015 USC *
 * All rights reserved.                                                  *
 *                                                                       *
 * Code author: Jernej Barbic                                            *
 * http://www.jernejbarbic.com/code                                      *
 *                                                                       *
 * Research: Jernej Barbic, Fun Shing Sin, Daniel Schroeder,             *
 *           Doug L. James, Jovan Popovic                                *
 *                                                                       *
 * Funding: National Science Foundation, Link Foundation,                *
 *          Singapore-MIT GAMBIT Game Lab,                               *
 *          Zumberge Research and Innovation Fund at USC                 *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of the BSD-style license that is            *
 * included with this library in the file LICENSE.txt                    *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the file     *
 * LICENSE.TXT for more details.                                         *
 *                                                                       *
 *************************************************************************/
/* Deleted unwanted routines -- Santidan 24-Mar-2016 */

#include "volumetricMeshLoader.h"
#include "tetMesh.h"

// for faster parallel loading of multimesh binary files, enable the -fopenmp -DUSE_OPENMP macro line in the Makefile-header file (see also documentation)

#ifdef USE_OPENMP
#include <omp.h>
#endif

VolumetricMesh * VolumetricMeshLoader::load(const char * filename, VolumetricMesh::fileFormatType fileFormat, int verbose)
{
  VolumetricMesh::elementType elementType_ = VolumetricMesh::getElementType(filename, fileFormat);
  if (elementType_ == VolumetricMesh::INVALID)
  {
    return NULL;
  }

  VolumetricMesh * volumetricMesh = NULL;

  switch (fileFormat)
  {
    case VolumetricMesh::ASCII:
    {
      if (elementType_ == TetMesh::elementType())
        volumetricMesh = new TetMesh(filename, VolumetricMesh::ASCII, verbose);

    }
  	break;

    default:
    {
      printf("Error in VolumetricMeshLoader: invalid file format.\n");
      return NULL;
    }
  }

  return volumetricMesh;
}
