/*
 * ************************************************************************
 *                                                                       *
 * Vega FEM Simulation Library Version 2.2                               *
 *                                                                       *
 * "volumetricMesh" library , Copyright (C) 2007 CMU, 2009 MIT, 2015 USC *
 * All rights reserved.                                                  *
 *                                                                       *
 * Code authors: Jernej Barbic, Yijing Li                                *
 * http://www.jernejbarbic.com/code                                      *
 *                                                                       *
 * Research: Jernej Barbic, Fun Shing Sin, Daniel Schroeder,             *
 *           Doug L. James, Jovan Popovic                                *
 *                                                                       *
 * Funding: National Science Foundation, Link Foundation,                *
 *          Singapore-MIT GAMBIT Game Lab,                               *
 *          Zumberge Research and Innovation Fund at USC                 *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of the BSD-style license that is            *
 * included with this library in the file LICENSE.txt                    *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the file     *
 * LICENSE.TXT for more details.                                         *
 *                                                                       *
 *************************************************************************
 * volumetricMesh.cpp modified
 *
 *  Created on: Mar 24, 2016
 *      Author: santidan
 */

#include <float.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <map>
#include "volumetricMeshParser.h"
#include "volumetricMesh.h"
using namespace std;

//double VolumetricMesh::density_default = 1000;
//double VolumetricMesh::E_default = 1E9;
//double VolumetricMesh::nu_default = 0.45;

// parses the mesh, and returns the string corresponding to the element type
VolumetricMesh::VolumetricMesh(const char * filename, fileFormatType fileFormat, int numElementVertices_, elementType * elementType_, int verbose): numElementVertices(numElementVertices_)
{
	numVertices = 0;
	numElements=0;
    elements =0;
    vertices = 0;
    temp=INVALID;
  if (verbose)
  {
    printf("Opening file %s.\n", filename); fflush(NULL);
  }

  switch (fileFormat)
  {
    case ASCII:
      loadFromAscii(filename, elementType_);
    break;

    default:
      printf("Error in VolumetricMesh::VolumetricMesh: file format is unknown.\n");
    break;
  }
}

VolumetricMesh::~VolumetricMesh()
{
	for (int i = 0; i < numVertices; i++)
		delete (vertices[i]);
	free(vertices);

	for (int i = 0; i < numElements; i++)
		free(elements[i]);
	free(elements);

}

VolumetricMesh::VolumetricMesh(const VolumetricMesh & volumetricMesh)
{
	temp = INVALID;
	numVertices = volumetricMesh.numVertices;
	vertices = (Vec3d**) malloc(sizeof(Vec3d*) * numVertices);
	for (int i = 0; i < numVertices; i++)
		vertices[i] = new Vec3d(*(volumetricMesh.vertices[i]));

	numElementVertices = volumetricMesh.numElementVertices;
	numElements = volumetricMesh.numElements;
	elements = (int**) malloc(sizeof(int*) * numElements);
	for (int i = 0; i < numElements; i++)
	{
		elements[i] = (int*) malloc(sizeof(int) * numElementVertices);
		for (int j = 0; j < numElementVertices; j++)
			elements[i][j] = (volumetricMesh.elements)[i][j];
	}
}

VolumetricMesh::elementType VolumetricMesh::getElementTypeASCII(
		const char * filename)
{
	//printf("Parsing %s... (for element type determination)\n",filename);fflush(NULL);
	elementType elementType_;

	// parse the .veg file
	VolumetricMeshParser volumetricMeshParser;
	elementType_ = INVALID;

	if (volumetricMeshParser.open(filename) != 0)
	{
		printf("Error: could not open file %s.\n", filename);
		return elementType_;
	}

	char lineBuffer[1024];
	while (volumetricMeshParser.getNextLine(lineBuffer, 0, 0) != NULL)
	{
		//printf("%s\n", lineBuffer);

		// seek for *ELEMENTS
		if (strncmp(lineBuffer, "*ELEMENTS", 9) == 0)
		{
			// parse element type
			if (volumetricMeshParser.getNextLine(lineBuffer) != NULL)
			{
				volumetricMeshParser.removeWhitespace(lineBuffer);

				if (strncmp(lineBuffer, "TET", 3) == 0)
					elementType_ = TET;
				else if (strncmp(lineBuffer, "CUBIC", 5) == 0)
					elementType_ = CUBIC;
				else
				{
					printf("Error: unknown mesh type %s in file %s\n",
							lineBuffer, filename);
					return elementType_;
				}
			}
			else
			{
				printf(
						"Error (getElementType): file %s is not in the .veg format. Offending line:\n%s\n",
						filename, lineBuffer);
				return elementType_;
			}
		}
	}

	volumetricMeshParser.close();

	if (elementType_ == INVALID)
		printf("Error: could not determine the mesh type in file %s. File may not be in .veg format.\n", filename);

	return elementType_;
}

void VolumetricMesh::loadFromAscii(const char * filename, elementType * elementType_, int verbose)
{
	// create buffer for element vertices
	vector<int> elementVerticesBuffer(numElementVertices);
	int * v = &elementVerticesBuffer[0];

	// parse the .veg file
	VolumetricMeshParser volumetricMeshParser;

	if (volumetricMeshParser.open(filename) != 0)
	{
		printf("Error: could not open file %s.\n", filename);
		throw 1;
	}

	// === First pass: parse vertices and elements, and count the number of materials, sets and regions  ===

	int countNumVertices = 0;
	int countNumElements = 0;

	numElements = -1;
	*elementType_ = INVALID;
	int parseState = 0;
	char lineBuffer[1024];

	int oneIndexedVertices = 1;
	int oneIndexedElements = 1;
	while (volumetricMeshParser.getNextLine(lineBuffer, 0, 0) != NULL)
	{
		//lineBuffer now contains the next line
		//printf("%s\n", lineBuffer);

		// find *VERTICES
		if ((parseState == 0) && (strncmp(lineBuffer, "*VERTICES", 9) == 0))
		{
			parseState = 1;

			if (volumetricMeshParser.getNextLine(lineBuffer, 0, 0) != NULL)
			{
				// format is numVertices, 3, 0, 0
				sscanf(lineBuffer, "%d", &numVertices);  // ignore 3, 0, 0
				vertices = (Vec3d**) malloc(sizeof(Vec3d*) * numVertices);
			}
			else
			{
				printf(
						"Error: file %s is not in the .veg format. Offending line:\n%s\n",
						filename, lineBuffer);
				throw 2;
			}

			continue;
		}

		// find *ELEMENTS
		if ((parseState == 1) && (strncmp(lineBuffer, "*ELEMENTS", 9) == 0))
		{
			parseState = 2;

			// parse element type
			if (volumetricMeshParser.getNextLine(lineBuffer) != NULL)
			{
				volumetricMeshParser.removeWhitespace(lineBuffer);

				if (strncmp(lineBuffer, "TET", 3) == 0)
					*elementType_ = TET;
				else if (strncmp(lineBuffer, "CUBIC", 5) == 0)
					*elementType_ = CUBIC;
				else
				{
					printf("Error: unknown mesh type %s in file %s\n",
							lineBuffer, filename);
					throw 3;
				}
			}
			else
			{
				printf(
						"Error: file %s is not in the .veg format. Offending line:\n%s\n",
						filename, lineBuffer);
				throw 4;
			}

			// parse the number of elements
			if (volumetricMeshParser.getNextLine(lineBuffer, 0, 0) != NULL)
			{
				// format is: numElements, numElementVertices, 0
				sscanf(lineBuffer, "%d", &numElements); // only use numElements; ignore numElementVertices, 0
				elements = (int**) malloc(sizeof(int*) * numElements);
			}
			else
			{
				printf(
						"Error: file %s is not in the .veg format. Offending line:\n%s\n",
						filename, lineBuffer);
				throw 5;
			}

			continue;
		}

		if ((parseState == 2) && (lineBuffer[0] == '*'))
		{
			parseState = 3; // end of elements
		}

		if (parseState == 1)
		{
			// read the vertex position
			if (countNumVertices >= numVertices)
			{
				printf("Error: mismatch in the number of vertices in %s.\n",
						filename);
				throw 6;
			}

			// ignore space, comma or tab
			char * ch = lineBuffer;
			while ((*ch == ' ') || (*ch == ',') || (*ch == '\t'))
				ch++;

			int index;
			sscanf(ch, "%d", &index);
			// seek next separator
			while ((*ch != ' ') && (*ch != ',') && (*ch != '\t') && (*ch != 0))
				ch++;

			if (index == 0)
				oneIndexedVertices = 0; // input mesh has 0-indexed vertices

			double pos[3];
			for (int i = 0; i < 3; i++)
			{
				// ignore space, comma or tab
				while ((*ch == ' ') || (*ch == ',') || (*ch == '\t'))
					ch++;

				if (*ch == 0)
				{
					printf("Error parsing line %s in file %s.\n", lineBuffer,
							filename);
					throw 7;
				}

				sscanf(ch, "%lf", &pos[i]);

				// seek next separator
				while ((*ch != ' ') && (*ch != ',') && (*ch != '\t')
						&& (*ch != 0))
					ch++;
			}

			vertices[countNumVertices] = new Vec3d(pos);
			countNumVertices++;
		}

		if (parseState == 2)
		{
			// read the element vertices
			if (countNumElements >= numElements)
			{
				printf("Error: mismatch in the number of elements in %s.\n",
						filename);
				throw 8;
			}

			// ignore space, comma or tab
			char * ch = lineBuffer;
			while ((*ch == ' ') || (*ch == ',') || (*ch == '\t'))
				ch++;

			int index;
			sscanf(ch, "%d", &index);

			if (index == 0)
				oneIndexedElements = 0; // input mesh has 0-indexed elements

			// seek next separator
			while ((*ch != ' ') && (*ch != ',') && (*ch != '\t') && (*ch != 0))
				ch++;

			for (int i = 0; i < numElementVertices; i++)
			{
				// ignore space, comma or tab
				while ((*ch == ' ') || (*ch == ',') || (*ch == '\t'))
					ch++;

				if (*ch == 0)
				{
					printf("Error parsing line %s in file %s.\n", lineBuffer,
							filename);
					throw 9;
				}

				sscanf(ch, "%d", &v[i]);

				// seek next separator
				while ((*ch != ' ') && (*ch != ',') && (*ch != '\t')
						&& (*ch != 0))
					ch++;
			}

			// if vertices were 1-numbered in the .veg file, convert to 0-numbered
			for (int k = 0; k < numElementVertices; k++)
				v[k] -= oneIndexedVertices;

			elements[countNumElements] = (int*) malloc(
					sizeof(int) * numElementVertices);
			for (int j = 0; j < numElementVertices; j++)
				elements[countNumElements][j] = v[j];

			countNumElements++;
		}
	}

	if (numElements < 0)
	{
		printf(
				"Error: incorrect number of elements.  File %s may not be in the .veg format.\n",
				filename);
		throw 10;
	}

	// === Second pass: parse materials, sets and regions ===

	volumetricMeshParser.rewindToStart();
	volumetricMeshParser.close();
}
VolumetricMesh::elementType VolumetricMesh::getElementType(const char * filename, fileFormatType fileFormat)
{
	switch (fileFormat)
	{
	case ASCII:
		return VolumetricMesh::getElementTypeASCII(filename);
	break;

	default:
		printf("Error: the file format %d is unknown. \n", fileFormat);
		exit(0);
	break;
	}
}
