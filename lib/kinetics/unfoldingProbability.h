/*
 * unfoldingProbability.h
 *
 *  Created on: May 17, 2016
 *      Author: santidan
 */
#include "spline.h"

#ifndef KINETICS_UNFOLDINGPROBABILITY_H_
#define KINETICS_UNFOLDINGPROBABILITY_H_

//double getLangevinFunction( double x);
//double getLInverse(const double x,const std::vector<double>& Lx,const std::vector<double>& Lf);
//double getLInverse(const double x);
double getProbabilityofContact(const double lambda, const int n, const int l);
double getProbabilityDistributionFunctionFJC(const double lambda, const int n);
double bellModelforce(const double gamma0,const double b,const double lambda,const int n);
double getPu(const double dt,const double pu0, const double lambda,const int nSegments,
		const int lSegments,const double gamma0,const double kuhnLength);

#endif /* KINETICS_UNFOLDINGPROBABILITY_H_ */
