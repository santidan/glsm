/*
 * -----------------------------------------------------------------
 * $Revision: 4272 $
 * $Date: 2014-12-02 11:19:41 -0800 (Tue, 02 Dec 2014) $
 * -----------------------------------------------------------------
 * Programmer(s): Scott D. Cohen, Alan C. Hindmarsh, and
 *                Radu Serban @ LLNL
 * -----------------------------------------------------------------
 * Example problem:
 *
 * The following is a simple example problem, with the coding
 * needed for its solution by CVODES. The problem is from chemical
 * kinetics, and consists of the following three rate equations:
 *    dy1/dt = -p1*y1 + p2*y2*y3
 *    dy2/dt =  p1*y1 - p2*y2*y3 - p3*(y2)^2
 *    dy3/dt =  p3*(y2)^2
 * on the interval from t = 0.0 to t = 4.e10, with initial
 * conditions y1 = 1.0, y2 = y3 = 0. The reaction rates are: p1=0.04,
 * p2=1e4, and p3=3e7. The problem is stiff.
 * This program solves the problem with the BDF method, Newton
 * iteration with the CVODES dense linear solver, and a
 * user-supplied Jacobian routine.
 * It uses a scalar relative tolerance and a vector absolute
 * tolerance.
 * Output is printed in decades from t = .4 to t = 4.e10.
 * Run statistics (optional outputs) are printed at the end.
 *
 * Optionally, CVODES can compute sensitivities with respect to the
 * problem parameters p1, p2, and p3.
 * The sensitivity right hand side is given analytically through the
 * user routine fS (of type SensRhs1Fn).
 * Any of three sensitivity methods (SIMULTANEOUS, STAGGERED, and
 * STAGGERED1) can be used and sensitivities may be included in the
 * error test or not (error control set on TRUE or FALSE,
 * respectively).
 *
 * Execution:
 *
 * If no sensitivities are desired:
 *    % cvsRoberts_FSA_dns -nosensi
 * If sensitivities are to be computed:
 *    % cvsRoberts_FSA_dns -sensi sensi_meth err_con
 * where sensi_meth is one of {sim, stg, stg1} and err_con is one of
 * {t, f}.
 * -----------------------------------------------------------------
 */

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cvodes/cvodes.h>           /* prototypes for CVODES fcts. and consts. */
#include <cvodes/cvodes_dense.h>     /* prototype for CVDENSE fcts. and constants */
#include <nvector/nvector_serial.h>  /* defs. of serial NVECTOR fcts. and macros  */
#include <sundials/sundials_types.h> /* def. of type realtype */
#include <sundials/sundials_math.h>  /* definition of ABS */
#include "spline.h"
#include "unfoldingProbability.h"
#include "gLSM.h"
/* Accessor macros */

#define Ith(v,i)    NV_Ith_S(v,i-1)       /* i-th vector component i=1..NEQ */
#define IJth(A,i,j) DENSE_ELEM(A,i-1,j-1) /* (i,j)-th matrix component i,j=1..NEQ */

/* Problem Constants */

#define NEQ   1             /* number of equations  */
#define Y1    RCONST(0.0)   /* initial y components */
#define Y2    RCONST(0.0)
#define Y3    RCONST(0.0)
#define RTOL  RCONST(1e-8)  /* scalar relative tolerance */
#define ATOL1 RCONST(1e-8)  /* vector absolute tolerance components */
#define ATOL2 RCONST(1e-12)
#define ATOL3 RCONST(1e-12)
#define T0    RCONST(0.0)   /* initial time */
#define T1    RCONST(0.4)   /* first output time */
#define TMULT RCONST(1.0)  /* RCONST(10.0) output time factor */
#define NOUT  1            /*12 number of output times */

#define NP    2             /* number of problem parameters */
#define NS    3             /* number of sensitivities computed */

#define ZERO  RCONST(0.0)

/* Type : UserData */

typedef struct {
  realtype p[2];           /* problem parameters */
} *UserData;

/* Prototypes of functions by CVODES */

static int f(realtype t, N_Vector y, N_Vector ydot, void *user_data);

static int Jac(long int N, realtype t,
               N_Vector y, N_Vector fy,
               DlsMat J, void *user_data,
               N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);

//static int fS(int Ns, realtype t, N_Vector y, N_Vector ydot,
//              int iS, N_Vector yS, N_Vector ySdot,
//              void *user_data, N_Vector tmp1, N_Vector tmp2);

static int ewt(N_Vector y, N_Vector w, void *user_data);

/* Prototypes of private functions */

//static void ProcessArgs(int argc, char *argv[],
//                        booleantype *sensi, int *sensi_meth,
//                        booleantype *err_con);
//static void WrongArgs(char *name);
//static void PrintOutput(void *cvode_mem, realtype t, N_Vector u);
//static void PrintOutputS(N_Vector *uS);
//static void PrintFinalStats(void *cvode_mem, booleantype sensi);
static int check_flag(void *flagvalue, char *funcname, int opt);

/*
 *--------------------------------------------------------------------
 * MAIN PROGRAM
 *--------------------------------------------------------------------
 */

double getPu(const double dt,const double pu0,const double lambda,const int nSegments,
		const int lSegments,const double gamma0,const double kuhnLength)
{
  void *cvode_mem;
  UserData data;
  realtype t, tout;
  N_Vector y;
  int iout, flag;
  N_Vector *yS;

//std::ofstream pContactFile;
//pContactFile.open("probContact_n4_l4_Interpolation.txt");
//while (lambda < 2.01){
//lambda += 0.001;
  //if ((lambda/sqrt(nSegments)) >= 1.01)
  //    break;
  cvode_mem = NULL;
  data      = NULL;
  y         = NULL;
  yS        = NULL;

  /* Process arguments */
  //ProcessArgs(argc, argv, &sensi, &sensi_meth, &err_con);

  /* User data structure */
  data = (UserData) malloc(sizeof *data);
  if (check_flag((void *)data, const_cast<char*> ("malloc"), 2)) return(1);
  realtype kf = 1.0*getProbabilityofContact(lambda,nSegments,lSegments);
  //pContactFile << lambda << "  " << kf << std::endl;
  realtype kr = (1.0e-5)*bellModelforce(gamma0,kuhnLength,lambda,nSegments);
  data->p[0] = kf;
  data->p[1] = kr; //RCONST(1.0e-5);
  //data->p[2] = RCONST(3.0e7);
  //printf("kf: %12.4e Pc: %.5f\n", data->p[0],kf);
  /* Initial conditions */
  y = N_VNew_Serial(NEQ);
  if (check_flag((void *)y,const_cast<char*> ("N_VNew_Serial"), 0)) return(1);

  Ith(y,1) = pu0; //Y1;


  /* Create CVODES object */
  cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
  if (check_flag((void *)cvode_mem,const_cast<char*> ("CVodeCreate"), 0)) return(1);

  /* Allocate space for CVODES */
  flag = CVodeInit(cvode_mem, f, T0, y);
  if (check_flag(&flag,  const_cast<char*> ("CVodeInit"), 1)) return(1);

  /* Use private function to compute error weights */
  flag = CVodeWFtolerances(cvode_mem, ewt);
  if (check_flag(&flag, const_cast<char*> ("CVodeSetEwtFn"), 1)) return(1);

  /* Attach user data */
  flag = CVodeSetUserData(cvode_mem, data);
  if (check_flag(&flag, const_cast<char*> ("CVodeSetUserData"), 1)) return(1);

  /* Attach linear solver */
  flag = CVDense(cvode_mem, NEQ);
  if (check_flag(&flag,const_cast<char*> ("CVDense"), 1)) return(1);

  flag = CVDlsSetDenseJacFn(cvode_mem, Jac);
  if (check_flag(&flag,const_cast<char*> ("CVDlsSetDenseJacFn"), 1)) return(1);



//  for (iout=1, tout=T1; iout <= NOUT; iout++, tout *= TMULT)
  for (iout=1, tout=dt; iout <= NOUT; iout++, tout *= TMULT)
  {
    flag = CVode(cvode_mem, tout, y, &t, CV_NORMAL);
    if (check_flag(&flag, const_cast<char*> ("CVode"), 1)) break;
  }
  double pu1 = Ith(y,1);
  /* Print final statistics */
  //PrintFinalStats(cvode_mem, sensi);
  // std::cout << lambda << " " << pu1 << std::endl;

  /* Free memory */

  N_VDestroy_Serial(y);                    /* Free y vector */
  free(data);                              /* Free user data */
  CVodeFree(&cvode_mem);                   /* Fre
  e CVODES memory */
  return(pu1);
}

/*
 *--------------------------------------------------------------------
 * FUNCTIONS CALLED BY CVODES
 *--------------------------------------------------------------------
 */

/*
 * f routine. Compute f(t,y).
 */

static int f(realtype t, N_Vector y, N_Vector ydot, void *user_data)
{
  realtype y1, yd1; //, y2, y3, yd1, yd3;
  UserData data;
  realtype p1, p2; //, p3;

  y1 = Ith(y,1); // y2 = Ith(y,2); y3 = Ith(y,3);
  data = (UserData) user_data;
  p1 = data->p[0]; p2 = data->p[1];// p3 = data->p[2];

  yd1 = Ith(ydot,1) = -p1*y1 + p2*(1.0 - y1); //*y2*y3;
  //yd3 = Ith(ydot,3) = p3*y2*y2;
  //      Ith(ydot,2) = -yd1 - yd3;

  return(0);
}


/*
 * Jacobian routine. Compute J(t,y).
 */

static int Jac(long int N, realtype t,
               N_Vector y, N_Vector fy,
               DlsMat J, void *user_data,
               N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
{
  realtype y1; //, y2, y3;
  UserData data;
  realtype p1, p2; //, p3;

  y1 = Ith(y,1); //y2 = Ith(y,2); y3 = Ith(y,3);
  data = (UserData) user_data;
	p1 = data->p[0]; p2 = data->p[1]; //p3 = data->p[2];

  IJth(J,1,1) = -p1-p2;  //IJth(J,1,2) = p2*y3;          IJth(J,1,3) = p2*y2;
 // IJth(J,2,1) =  p1;  IJth(J,2,2) = -p2*y3-2*p3*y2; IJth(J,2,3) = -p2*y2;
                    //  IJth(J,3,2) = 2*p3*y2;

  return(0);
}
/*
 * EwtSet function. Computes the error weights at the current solution.
 */

static int ewt(N_Vector y, N_Vector w, void *user_data)
{
  int i;
  realtype yy, ww, rtol, atol[3];

  rtol    = RTOL;
  atol[0] = ATOL1;
  atol[1] = ATOL2;
  atol[2] = ATOL3;

  for (i=1; i<=3; i++) {
    yy = Ith(y,i);
    ww = rtol * SUNRabs(yy) + atol[i-1];
    if (ww <= 0.0) return (-1);
    Ith(w,i) = 1.0/ww;
  }

  return(0);
}

/*
 *--------------------------------------------------------------------
 * PRIVATE FUNCTIONS
 *--------------------------------------------------------------------
 */

static int check_flag(void *flagvalue, char *funcname, int opt)
{
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if (opt == 0 && flagvalue == NULL) {
    fprintf(stderr,
            "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
	    funcname);
    return(1); }

  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
      fprintf(stderr,
              "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
	      funcname, *errflag);
      return(1); }}

  /* Check if function returned NULL pointer - no memory allocated */
  else if (opt == 2 && flagvalue == NULL) {
    fprintf(stderr,
            "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
	    funcname);
    return(1); }

  return(0);
}
//double getLangevinFunction(double x)
//{
//  return (1.0/tanh(x) - 1.0/x );
//}

//double getLInverse(const double x,const std::vector<double>& Lx,const std::vector<double>& Lf)
//{
//	if (x < 1.)
//	{
//		tk:: spline s;
//		s.set_points(Lf,Lx);
//            //double var =x*(3.0 - 2.6*x + 0.7*x*x)/((1.0 - x)*(1.0 + 0.1*x)); //R. Jedynak
//            //double var =x*(x*x -3*x + 3.0)/(1.0 - x);
//	    //double var = 3*x*((35.0 - 12.0*x*x)/(35.0- 33.0*x*x)); //Pade Approximant O(x^7)
//	   // double var = (3.*x)/(1 - (3*pow(x,2))/5. - (36*pow(x,4))/175. - (108*pow(x,6))/875. - (5508*pow(x,8))/67375. -  (162324*pow(x,10))/3.128125e6);
//	   double var = s(x);
//	    //std::cout << "var:" << x << " " << var << std::endl;
//	    //return(var); //R. Jedynak
//	    return(var); //R. Jedynak
//	}
//	//Jedynak, R. (2015). Rheologica Acta 54 (1): 29–39
//	//"Approximation of the inverse Langevin function revisited".
//	return(x*(3.0 - 2.6*x + 0.7*x*x)/((1.0 - x)*(1.0 + 0.1*x))); //R. Jedynak
//	//return(1./(1.-x));
//	//return (3*x*((35.0 - 12.0*x*x)/(35.0- 33.0*x*x)));
//}

//double getLInverse(const double x)
//{
//	if (x < 1.)
//	{
//       //double var =x*(3.0 - 2.6*x + 0.7*x*x)/((1.0 - x)*(1.0 + 0.1*x)); //R. Jedynak
//       //double var =x*(x*x -3*x + 3.0)/(1.0 - x);
//	   //double var = 3*x*((35.0 - 12.0*x*x)/(35.0- 33.0*x*x)); //Pade Approximant O(x^7)
//	   // double var = (3.*x)/(1 - (3*pow(x,2))/5. - (36*pow(x,4))/175. - (108*pow(x,6))/875. - (5508*pow(x,8))/67375. -  (162324*pow(x,10))/3.128125e6);
//	   double var = splineInterpolation(x);
//	    //std::cout << "var:" << x << " " << var << std::endl;
//	    //return(var); //R. Jedynak
//	    return(var); //R. Jedynak
//	}
//	//Jedynak, R. (2015). Rheologica Acta 54 (1): 29–39
//	//"Approximation of the inverse Langevin function revisited".
//	return(x*(3.0 - 2.6*x + 0.7*x*x)/((1.0 - x)*(1.0 + 0.1*x))); //R. Jedynak
//	//return(1./(1.-x));
//	//return (3*x*((35.0 - 12.0*x*x)/(35.0- 33.0*x*x)));
//}

double getProbabilityDistributionFunctionFJC(const double lambda, const int n)
{
	double x = lambda/sqrt(n);
	if (lambda > (0.99999999*sqrt(n)))
		return 0;
	//std::cout << "pdf:" << n << " " << lambda << std::endl;
	double invL = getLInverseTemp(x);
	double numerator = pow(invL,2.0);
//	pow(2.0*PI*n*kuhnLength*kuhnLength,1.5)
	double denominator = pow(n,1.5)*x*sqrt(1.0 - pow(invL/sinh(invL),2.0));
	double result =	(numerator/denominator)*pow((sinh(invL)/invL)*exp(-x*invL),n);
	return (result);
}

double getProbabilityofContact(const double lambda, const int n, const int l)
{
	double result;
	result = pow(l,-1.5)*getProbabilityDistributionFunctionFJC(lambda,n)/getProbabilityDistributionFunctionFJC(lambda,n+l);
	return (result);
}

double bellModelforce(const double gamma0,const double b,const double lambda,const int n)
{
      double x = lambda/sqrt(n);
      double result;
	if (lambda > (0.96*sqrt(n)))
		return (exp((gamma0/b)*getLInverseTemp(0.96)));
      result = exp((gamma0/b)*getLInverseTemp(x));
      return(result);
}




