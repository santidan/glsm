/** @file eig3.h
 *  @brief Eigen-decomposition for symmetric 3x3 real matrices.
 *
 *  Function copied as is from Vega FEM 2.2
 *  which copied from the public domain Java library JAMA.
 *  For details please look at Vega FEM 2.2.
 *
 *  @bug No known bugs
 */
/* Eigen-decomposition for symmetric 3x3 real matrices.
   Public domain, copied from the public domain Java library JAMA. */

#ifndef _eig_h

/* Symmetric matrix A => eigenvectors in columns of V, corresponding
   eigenvalues in d. */
/** @brief carries out eigen decomposition
 * @param A Symmetric matrix
 * @param V eigenvectors in columns
 * @param d eigenvalues
 * @return Void.
 */
void eigen_decomposition(double A[3][3], double V[3][3], double d[3]);

#endif
