/*
 * main.cpp
 *
 *  Created on: Apr 8, 2016
 *      Author: santidan
 */
/** @file generateTetMeshFromTetgenInput.cpp
 *  @brief gLSM Implementation
 *
 *  This file contains the main() function.
 *  This is where we read the Tetgen input,
 *  run the code and get the output for gLSM
 *
 *  @date March 24, 2016
 *  @author Santidan Biswas
 *  @bug No known bugs yet
 */

/* -- Includes -- */
#include <stdlib.h>
#include <vector>
#include <string>
#include <cassert>
#include <float.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <math.h>
#include "volumetricMeshLoader.h"
#include "minivector.h"
#include "tetMesh.h"
#include "spline.h" //spline interpolation from lib/interpolation
#include "unfoldingProbability.h"
#include "gLSM.h"

using namespace std;

const double PI = 3.141592653589793238463;
const double kuhnLength = 1.0;
const double SVD_singularValue_eps = 1e-14;

/**@brief Main() function
 *
 * Data parsing from tetgen input.
 * Time evolution of the gel.
 * Output values.
 *
 * @return should not return
 */



//instantiate spline interpolation class globally so that data is available
//To do an interpolation for inverse langevin function
tk:: spline splineMain;
VolumetricMesh * volumetricMesh = NULL;
TetMesh * tetMesh = NULL;
int main(int argc, char** argv)
{
	int numTimeSteps = 1100;
	double deltaT = 0.01;
	double temperature = 14.5;
	double maxTemperature = 45.1;
	const double c0 = 0.00054;
	const double lambda0 = 1000.0;
    const int nKuhnSegments = 4;
    const int lKuhnSegments = 3;
	const double phi0 = 0.128571*(1. + lKuhnSegments/double (nKuhnSegments) ); //volume fraction of polymer in undeformed sample
    const double gamma0 =2.15;
    double pu0 = 0.0;
    //char opfileName[30];
    int opfileCount ;
    //Generating a table data for Langevin function
    int n = 5000;
    std::vector<double> Langevin_f(n);
    std::vector<double> Langevin_x(n);
    Langevin_x[0]=0.0;
    Langevin_f[0]=0.0;
//    std::ofstream Lfunction;
//    Lfunction.open("LangevinFunction.txt");
    for (int i=1;i<n;++i)
    {
    Langevin_x[i] = Langevin_x[i-1] + 0.01;
     Langevin_f[i]=getLangevinFunction(Langevin_x[i]);
//     Lfunction <<Langevin_x[i] << " " << Langevin_f[i] << std::endl;
    }
//    Lfunction.close();
    //providing input set of points to spline interpolation  for LInverse
    splineMain.set_points(Langevin_f,Langevin_x);

//	if (argc < 2)
//	{
//		std::cout << "InputFileName: " << argv[0] << std::endl;
//		std::cout << "loads and prints the input mesh element" << std::endl;
//		return 1;
//	}
//	char * inputFileName = argv[1];
	/*
	 * Already provide the input file
	 */
	ofstream outputFile("/home/santidan/Desktop/volFJC_interpolated.txt");
	//ofstream outputFile1("/home/santidan/Desktop/probContact.txt");
	char inputFileName[96] = "inputs/cubeTetgen.veg";
	//char inputFileName[96] = "inputs/singleTet1.veg";
	//VolumetricMesh * volumetricMesh = NULL;
	volumetricMesh = VolumetricMeshLoader::load(inputFileName);
	//TetMesh * tetMesh;
	if (volumetricMesh->getElementType() == VolumetricMesh::TET)
		tetMesh = (TetMesh*) volumetricMesh;
	else
	{
		printf("Error: not a tet mesh.\n");
		exit(1);
	}
	int numElements = tetMesh->getNumElements();
	int numVertices = tetMesh->getNumVertices();
	int numElementVertices = tetMesh->getNumElementVertices();
	Vec3d verticesInitial[numVertices];
	for (int i = 0; i < numVertices; ++i)
	{
		Vec3d tmp = *(tetMesh->getVertex(i));
		verticesInitial[i] = tmp;//*(tetMesh->getVertex(i));
//		printf("vertex %d  %f %f %f\n", i, verticesInitial[i][0],
//				verticesInitial[i][1], verticesInitial[i][2]);
	}
	Vec3d vertices[numVertices];
//	for (int i = 0; i < numVertices; ++i)
//	{
//		vertices[i] = verticesInitial[i];
//	}
	printf("\n");
	double totVolume0 = 0.0;
//  find initial volume of every element
//	double elementInitialVolume[numElements];
//	for (int i = 0; i < numElements; ++i)
//	{
//		printf("Element: %d  VertexIndex: ", i);
//		elementInitialVolume[i] = tetMesh->getElementVolume(i);
//		printf("\nVolume of tet Element %d : %.15f \n", i,
//				elementInitialVolume[i]);
//		totVolume0 += elementInitialVolume[i];
//	}
//	printf("\n");

	/*
	 *calculate the gradients of old L1, L2, L3, L4
	 *calculate elemental volume fraction
	 *calculate Flory-Huggins contribution, isotropic presure
	 */

	Vec3d qVel[numVertices];
	double totVertexVolume[numVertices];
	Vec3d initialVtx[numElementVertices];
	Vec3d currentVtx[numElementVertices];
	double isotropicPressure;
	double elementVolume = 0.0;
	/*
	 * time evolution of the gel
	 */
	double totVolume;
	double phi;
	double mobility;
	double elementVolume0 = 0.0;
    double avgPhi;
	Vec3d ** initGrads;
	initGrads = (Vec3d**) malloc(sizeof(Vec3d*) * numElementVertices);
	Vec3d ** grads;
	grads = (Vec3d**) malloc(sizeof(Vec3d*) * numElementVertices);
	Vec3d vtxInitGrads;
	Vec3d vtxInitGrads2;
	Vec3d gradLNew;
	//outputFile.open ("/home/santi/Desktop/volume.txt");
	//temperature loop
	do
	{
		temperature += 0.5;
	double chiValue = chi0(temperature);
	cout << "chiValue: " << chiValue <<endl;
	double lambda = 1.0;
	double probUnfolding = 0.0;
	//Vec3d vertices[numVertices];
	// Re-initialize vertices
	for (int i = 0; i < numVertices; ++i)
	{
		vertices[i] = verticesInitial[i];
	}
	//double termFJC = getZeta(sqrt(n));
	opfileCount = 0;
	for (int iTimestep = 0; iTimestep < numTimeSteps; ++iTimestep)
	{
		//printf("Timestep: %d\n",iTimestep);
		totVolume = 0.0;
		totVolume0 = 0.0;
		phi = 0.0;
		mobility = 0.0;
		avgPhi = 0.0;
		memset(totVertexVolume,0,sizeof(totVertexVolume));
		memset(qVel,0,sizeof(qVel));
		for (int iElement = 0; iElement < numElements; ++iElement) //element loop
		{
			for (int j = 0; j < numElementVertices; ++j)
			{
				int vIndex = tetMesh->getVertexIndex(iElement, j);
				initialVtx[j] = verticesInitial[vIndex];
				currentVtx[j] = vertices[vIndex];
			}
			elementVolume0 = tetMesh->getTetVolume(&initialVtx[0], &initialVtx[1],
					&initialVtx[2], &initialVtx[3]);
			elementVolume = tetMesh->getTetVolume(&currentVtx[0], &currentVtx[1],
					&currentVtx[2], &currentVtx[3]);

			//---------calculate principal stretches----------------------
		    Vec3d ds1 = currentVtx[0] - currentVtx[1];
		    Vec3d ds2 = currentVtx[0] - currentVtx[2];
		    Vec3d ds3 = currentVtx[0] - currentVtx[3];

		    Mat3d tmp(ds1[0], ds2[0], ds3[0], ds1[1], ds2[1], ds3[1], ds1[2], ds2[2], ds3[2]);

		    Vec3d dm1 = initialVtx[0] -initialVtx[1];
		    Vec3d dm2 = initialVtx[0] -initialVtx[2];
		    Vec3d dm3 = initialVtx[0] -initialVtx[3];
		    //dm1.print();
		    Mat3d tmpInitial(dm1[0], dm2[0], dm3[0], dm1[1], dm2[1], dm3[1], dm1[2], dm2[2], dm3[2]);
		   // printf("--- dm ---\n");
		    //tmp.print();
		    Mat3d dmInverses = inv(tmpInitial);
		    //printf("--- inv(dm) ---\n");
		    //dmInverses.print();
		    Mat3d Fs = tmp * dmInverses;
		    //printf("F =\n");
		    //Fs[el].print();
		    Mat3d Us, Vs;
		    /*
		      The deformation gradient has now been computed and is available in Fs[el]
		    */

		    // perform modified SVD on the deformation gradient
		    Mat3d & F = Fs;
		    Mat3d & U = Us;
		    Mat3d & V = Vs;
		    Vec3d Fhat ;
		    int modifiedSVD = 1;
		    if (SVD(F, U, Fhat, V, SVD_singularValue_eps, modifiedSVD) != 0)
		    {
		      printf("error in diagonalization, el=%d\n", iElement);
		    }
		    double I1 = pow((Fhat[0]*Fhat[0] + Fhat[1]*Fhat[1] +Fhat[2]*Fhat[2])/3.0,0.5) ;
            //cout << Fhat << " " << I1 << endl;
			//--------End of prinicpal stretch computation----------------------
			//cout << "computeGradL Initial vertex: " << endl;
			initGrads = tetMesh->computeGradL(&initialVtx[0], &initialVtx[1],
					&initialVtx[2], &initialVtx[3]);
			//cout << "computeGradL New vertex"  << endl;
			grads = tetMesh->computeGradL(&currentVtx[0], &currentVtx[1], &currentVtx[2],
					&currentVtx[3]);
			//calculate spring-force and pressure-force
			Vec3d latticeSpringForce;
			Vec3d pressureTerm;
			phi = phi0 * elementVolume0 / elementVolume;
			//double lambda =pow(elementVolume/elementVolume0,1.0/3.0);
			double avgElementStrain = I1/sqrt(double (nKuhnSegments)) ; //pow(3.0,-0.5)*pow((lambda*lambda + 2.0*phi0/(lambda*phi)),0.5)/sqrt(n);
			/*
			 * zeta(lambda/sqrt(n))
			 * unfolding (n +l) Kuhnsegments
			 * folded n Kuhn Segments
			 */
			double termFJC = getZeta(avgElementStrain);
			double termFJCnl = getZeta(I1/sqrt(double (nKuhnSegments + lKuhnSegments)));
			avgPhi += phi;
			/*
			 * added probability of unfolding -- time not passed hence steady state
			 * need to modify to pass time
			 */
			probUnfolding = getPu(deltaT,pu0, I1, nKuhnSegments, lKuhnSegments, gamma0, kuhnLength);
			//cout << probUnfolding << endl;
			/*
			 * pressure term from flory huggins and also premanent crosslink second term
			 * second term from permanent crosslink
			 * Bug:Fixed (added the zeta(n^-1/2) )
			 * Also added contributions from prob of unfolding,pu and folding,(1- pu)
			 */
//			isotropicPressure = getFloryHuggins(phi, chiValue) + (0.5 * c0 * (phi / phi0)*getZeta(1./sqrt(double(nKuhnSegments))));
			isotropicPressure = getFloryHuggins(phi, chiValue)
					+ (0.5 * c0 * phi / phi0)*(probUnfolding*getZeta(1./sqrt(double (nKuhnSegments + lKuhnSegments)))
			+ (1. - probUnfolding)*getZeta(1./sqrt(double(nKuhnSegments))));
			//cout << "isotropicPresure " << isotropicPressure << endl;
			mobility = lambda0 * (1.0 - phi) * pow((phi0 / phi), 0.5);
			for (int j = 0; j < numElementVertices; ++j) //vertex in element loop
			{

				vtxInitGrads =*initGrads[j];
				gradLNew = *grads[j];
				//printf("grads %d : %.5f   %.5f   %.5f\n", j, vtxInitGrads[0], vtxInitGrads[1], vtxInitGrads[2]);
				//printf("gradn %d : %.5f   %.5f   %.5f\n", j, gradLNew[0] , gradLNew[1], gradLNew[2]);
				latticeSpringForce = 0.0;
				pressureTerm = isotropicPressure * gradLNew;
				for (int k = 0; k < numElementVertices; ++k)
				{
					if (k == j)	continue;
					vtxInitGrads2 = *initGrads[k];
					latticeSpringForce += dot(vtxInitGrads,vtxInitGrads2) * (currentVtx[k] - currentVtx[j]);
//    		 printf("force: %d  %d\n", j, k);
					//cout << latticeSpringForce << endl;
				}
				/*
				 * Added prob of unfolding and folding contributions to gel elasticity
				 */
				qVel[tetMesh->getVertexIndex(iElement, j)] += (elementVolume
						* mobility)
						* (-c0 * termFJC*(1.0-probUnfolding)*latticeSpringForce
							 -c0 * termFJCnl*probUnfolding*latticeSpringForce
								+ ((phi0 / phi) * pressureTerm));
//				qVel[tetMesh->getVertexIndex(iElement, j)] += (elementVolume
//						* mobility)
//						* (-c0 * termFJC*latticeSpringForce
//								+ ((phi0 / phi) * pressureTerm));
				totVertexVolume[tetMesh->getVertexIndex(iElement, j)] += elementVolume;
				//sum[i][j]=latticeSpringForce + pressureTerm;
				//printf("force on vertex %d of element %d \n", j, iElement);
				//std::cout << /*qVel[0] /*/ totVertexVolume[0] << std::endl;
			}
			totVolume += elementVolume;
			totVolume0 +=elementVolume0;
			for (int j = 0; j < numElementVertices; ++j) //vertex in element loop
				{
				 delete initGrads[j];
				 delete grads[j];
				}
			 free(initGrads);
			 free(grads);
		}				//element loop
		for (int iVertices = 0; iVertices < numVertices; ++iVertices)
		{
			vertices[iVertices] += ((qVel[iVertices]/totVertexVolume[iVertices]) * deltaT);
			//std::cout << *vertices[iVertices] << std::endl ;
		}
		lambda = pow(elementVolume/elementVolume0,1.0/3.0);
//		probUnfolding = getPu(deltaT,pu0, lambda, nKuhnSegments, lKuhnSegments, gamma0, kuhnLength);
		pu0 = probUnfolding;
		/*
		 * Writing sequence of output to file for viewing with paraview
		 */
		if (iTimestep % 1000 ==0)
		{
			opfileCount += 1;
			//sprintf(opfileName, "T_%.2f_%02d.txt", temperature,opfileCount);
			stringstream stream;
			stream << fixed << setprecision(1) << temperature;
			string s = stream.str();
			stringstream stream1;
			stream1 << setfill('0') << setw(3) << opfileCount;
			string numfile = stream1.str();
		    string opfileName = "output/cube/cube_T_" + s + "_" + numfile + ".vtk";
		    const char * opFile = opfileName.c_str();
			outmesh2vtk(opFile,vertices);
			printf("%d  %f  %f  %f  %f \n", iTimestep, totVolume0, totVolume,pow((totVolume / totVolume0), 1. / 3.),avgPhi/numElements);
		}//if (iTimestep % 1000 ==0)
		//	outputFile << iTimestep << "  " << pow((totVolume / totVolume0), 1. / 3.) << " " << avgPhi/numElements << endl;
	} //iteration loop
	outputFile << temperature << " " << pow((totVolume / totVolume0), 1. / 3.) << " " << avgPhi/numElements << " " << probUnfolding << endl;
	}while (temperature < maxTemperature);


//	double stretch =1.0;
//	while(stretch < 2.4){
//
//		stretch += 0.01;
//
//		outputFile1 <<stretch << " " << getProbabilityofContact(stretch,5,2) << " "
//				<< getProbabilityofContact(stretch,5,3) << " "
//				<< getProbabilityofContact(stretch,5,5) << endl;
//	}
	outputFile.close();
	printf("\n********* Test run completed ******* \n");
	//delete[] vtxInitGrads;
	//delete[] gradLNew;
	free(verticesInitial);
	free(vertices);
	//delete verticesInitial;
	delete tetMesh;
	delete volumetricMesh;
	return 0;
} // end of main()


double getFloryHuggins(const double x, const double chi)
{
	double termFH = 0.0;
	termFH = -1.0*(x + log(1.0 - x) + ((chi + 0.518 * x) * x * x));
	//termFH = -(x + log (1.0 - x) + (0.338 + 0.518 * x) * x * x);
	return (termFH);
}

double getZeta(const double x)
{
	return((1.0/(3.0*x))*getLInverseTemp(x));
}



//double getLInverse(const double x)
//{
//	if (x < 1.01)
//	{
//		//return(3*x*((35.0 - 12.0*x*x)/(35.0- 33.0*x*x))); //Pade Approximant O(x^7)
//		return(s(x)); //interpolation
//	}
//	//Jedynak, R. (2015). Rheologica Acta 54 (1): 29–39
//	//"Approximation of the inverse Langevin function revisited".
//	return(x*(3.0 - 2.6*x + 0.7*x*x)/((1.0 - x)*(1.0 + 0.1*x))); //R. Jedynak
//	//return(1.0/(1.0-x));
//}
double getLInverseTemp(const double x)
{
	if (x < 1.01)
	{
		//return(3*x*((35.0 - 12.0*x*x)/(35.0- 33.0*x*x))); //Pade Approximant O(x^7)
		return(splineMain(x)); //interpolation
	}
	//Jedynak, R. (2015). Rheologica Acta 54 (1): 29–39
	//"Approximation of the inverse Langevin function revisited".
	return(x*(3.0 - 2.6*x + 0.7*x*x)/((1.0 - x)*(1.0 + 0.1*x))); //R. Jedynak
	//return(1.0/(1.0-x));
}
//double getProbabilityDistributionFunctionFJC(const double lambda, const int n)
//{
//	double x = lambda/sqrt(n);
//	if (x > 0.99999999)
//		return 0;
//	double invL = getLInverse(x);
//	double numerator = pow(invL,2.0);
// //	pow(2.0*PI*n*kuhnLength*kuhnLength,1.5)
//	double denominator = pow(n,1.5)*x*sqrt(1.0 - numerator*pow(1.0/sinh(invL),2.0));
//	double result =	(numerator/denominator)*pow((sinh(invL)/invL)*exp(-x*invL),n);
//	return (result);
//}

//double getProbabilityofContact(const double lambda, const int n, const int l)
//{
//	double result;
//	result = pow(l,-1.5)*getProbabilityDistributionFunctionFJC(lambda,n)/getProbabilityDistributionFunctionFJC(lambda,n+l);
//	return (result);
//}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// outmesh2vtk()    Save mesh to file in VTK Legacy format.                  //
//                                                                           //
// This function was contributed by Bryn Llyod from ETH, 2007.               //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

void outmesh2vtk(const char* vtkfilename,Vec3d *vtx)
{
  FILE *outfile;
  //const char *vtkfilename ="sample.vtk";
  double x, y, z;
  int n1, n2, n3, n4;
  int nnodes = 4;
  int celltype = 10;

  int NEL = tetMesh->getNumElements();

  int NN = tetMesh->getNumVertices();

  outfile = fopen(vtkfilename, "w");

  fprintf(outfile, "# vtk DataFile Version 2.0\n");
  fprintf(outfile, "Unstructured Grid\n");
  fprintf(outfile, "ASCII\n"); // BINARY
  fprintf(outfile, "DATASET UNSTRUCTURED_GRID\n");
  fprintf(outfile, "POINTS %d double\n", NN);
  for (int id=0; id <NN; id++){
//	  Vec3d tmp = *(tetMesh->getVertex(id));
//	  x = tmp[0];
//	  y = tmp[1];
//	  z = tmp[2];
	  x = vtx[id][0];
	  y = vtx[id][1];
	  z = vtx[id][2];
	  fprintf(outfile, "%.17g %.17g %.17g\n", x, y, z);
  }
  fprintf(outfile, "\n");
  fprintf(outfile, "CELLS %d %d\n", NEL, NEL*(4+1));
  for (int id=0; id <NEL; id++){
  	  n1 =  tetMesh->getVertexIndex(id,0);
  	  n2 = tetMesh->getVertexIndex(id,1);
  	  n3 = tetMesh->getVertexIndex(id,2);
  	  n4 = tetMesh->getVertexIndex(id,3);
      fprintf(outfile, "%d  %4d %4d %4d %4d\n", nnodes, n1, n2, n3, n4);
  }
  fprintf(outfile, "\n");

  fprintf(outfile, "CELL_TYPES %d\n", NEL);
  for(int tid=0; tid<NEL; tid++){
    fprintf(outfile, "%d\n", celltype);
  }
  fprintf(outfile, "\n");
  fclose(outfile);
  }
