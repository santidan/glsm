/*
 * gLSM.h
 *
 *  Created on: May 18, 2016
 *      Author: santidan
 */

#ifndef GLSM_H_
#define GLSM_H_
#include "minivector.h"
//#include <string>
//function to calculate pressure due to FloryHuggins
double getFloryHuggins(const double , const double );
/*
 * chi0 as determined in S. Hirotsu, J. Chem. Phys. 1991, vol. 95, 3949
 */
inline double chi0(double t) //t is temperature in centigrade
{
	double kB = 1.3807;
	double T0 = 273.15;
	double h = -12.46 * 100;
	double s = -4.717;
	return ((h - (T0 + t) * s) / (kB * (T0 + t)));
}
//function to calculate Langevin function
inline double getLangevinFunction( double x)
{
  return (1.0/tanh(x) - 1.0/x );

}
//function to calculate inverse Langevin function
//double getLInverse(const double);
double getLInverseTemp(const double);

double getZeta(const double);
//double getProbabilityDistributionFunctionFJC(const double lambda, const int n);
//double getProbabilityofContact(const double lambda, const int n, const int l);
void outmesh2vtk(const char *,Vec3d *);

#endif /* GLSM_H_ */
