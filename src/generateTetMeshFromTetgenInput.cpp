/** @file generateTetMeshFromTetgenInput.cpp
 *  @brief gLSM Implementation
 *
 *  This file contains the main() function.
 *  This is where we read the Tetgen input,
 *  run the code and get the output for gLSM
 *
 *  @date March 24, 2016
 *  @author Santidan Biswas
 *  @bug No known bugs yet
 */

/* -- Includes -- */
//#include <stdlib.h>
//#include <vector>
//#include <string>
//#include <cassert>
//#include <float.h>
//#include <math.h>
#include <iostream>
#include <stdio.h>
#include "volumetricMeshLoader.h"
#include "minivector.h"
#include "tetMesh.h"
using namespace std;
/**@brief Main() function
 *
 * Data parsing from tetgen input.
 * Time evolution of the gel.
 * Output values.
 *
 * @return should not return
 */

//function to calculate pressure due to FloryHuggins
double getFloryHuggins(double x);

int main(int argc, char** argv)
{
	/*  if ( argc < 2 )
	 {
	 std::cout << "InputFileName: " << argv[0] << std::endl;
	 std::cout << "loads and prints the input mesh element" << std::endl;
	 return 1;
	 }*/
	//char * inputFileName = argv[1];
	//char inputFileName[96] = "singleTet1.veg";
	/*
	 * Already provide the input file
	 */
	char inputFileName[96] = "inputs/cubeTetgen.veg";
	VolumetricMesh * volumetricMesh = NULL;
	volumetricMesh = VolumetricMeshLoader::load(inputFileName);
	TetMesh * tetMesh;
	if (volumetricMesh->getElementType() == VolumetricMesh::TET)
		tetMesh = (TetMesh*) volumetricMesh; // such down-casting is safe in Vega
	else
	{
		printf("Error: not a tet mesh.\n");
		exit(1);
	}
	//int ** elements;
	int numElements = tetMesh->getNumElements();
	//elements = (int**) malloc(sizeof(int*) * numElements);
	int numVertices = tetMesh->getNumVertices();
	int numElementVertices = tetMesh->getNumElementVertices();
	Vec3d ** verticesInitialPositions;
	verticesInitialPositions = (Vec3d**) malloc(sizeof(Vec3d*) * numVertices);
	verticesInitialPositions = (tetMesh->getVertices());
	int vertexIndex;
	const double phi0 = 0.192857; //volume fraction of polymer in undeformed sample
	const double c0 = 0.00054;
	const double lambda0 = 1.0;
	//int vertexIndex = tetMesh->getVertexIndex(numElements-1,tetMesh->getNumElementVertices()-1);
	/*  for(int i=0; i<numElements; i++)
	 {
	 //verticesInitialPositions[i] = Vec3d *(volumetricMesh->getVertex(i,4));
	 for(int j=0; j<4; j++)
	 {
	 vertexIndex = tetMesh->getVertexIndex(i,j);
	 printf("Element: %d    Index: %d \n",i, vertexIndex);
	 Vec3d * vtx = verticesInitialPositions[vertexIndex];
	 printf("verticesInitialPositions of Index: %.1f   %.1f   %.1f \n", vtx[0][0] , vtx[0][1], vtx[0][2]);
	 }
	 }*/
	if (volumetricMesh == NULL)
	{
		printf("Error: failed to load mesh. \n");
	}
	else
	{
		//printf("Success.\n");
		printf(
				"Success.\nNumber of vertices: %d .\nNumber of elements: %d .\nNumber of elementsVertices: %d .\n",
				tetMesh->getNumVertices(), tetMesh->getNumElements(),
				numElementVertices);
	}
	printf("\n");
	//find vertex in every element
	double elementInitialVolume[numElements];

	for (int i = 0; i < numElements; i++)
	{
		printf("Element: %d  VertexIndex: ", i);
		for (int j = 0; j < 4; j++)
		{
			vertexIndex = tetMesh->getVertexIndex(i, j);
			printf("%d   ", vertexIndex);
		}
		elementInitialVolume[i] = tetMesh->getElementVolume(i);
		printf("\nVolume of tet Element %d : %.15f \n", i,
				elementInitialVolume[i]);
	}
	printf("\n");
//
//	for (int i = 0; i < numVertices; i++)
//	{
//		Vec3d * vtx = verticesInitialPositions[i];
//		printf("Vertices %d : %.1f   %.1f   %.1f \n", i, vtx[0][0], vtx[0][1],
//				vtx[0][2]);
//	}
//
//	// Calculate the barycentric coordinates
//
//	double * barycentricWeights;
//	barycentricWeights = (double*) malloc(sizeof(double) * numElementVertices);
//	printf("Barycentric Coordinates: \n");
//	for (int i = 0; i < numElements; i++)
//	{
//
//		for (int j = 0; j < 4; j++)
//		{
//			printf("Element: %d  VertexIndex: %d \n", i, j);
//			Vec3d pos = *verticesInitialPositions[tetMesh->getVertexIndex(i, j)];
//			/*    	 Vec3d pos = Vec3d(targetLocations[3*i+0],
//			 targetLocations[3*i+1],
//			 targetLocations[3*i+2]);*/
//			tetMesh->computeBarycentricWeights(i, pos, barycentricWeights);
//			printf("weights: %.5f %.5f %.5f %.5f\n", barycentricWeights[0],
//					barycentricWeights[1], barycentricWeights[2],
//					barycentricWeights[3]);
//		}
//	}
	/*
	 *calculate the gradients of old L1, L2, L3, L4
	 *calculate elemental volume fraction
	 *calculate Flory-Huggins contribution, isotropic presure
	 */
	Vec3d ** initGrads;
	initGrads = (Vec3d**) malloc(sizeof(Vec3d*) * 4);
	Vec3d ** grads;
	Vec3d qVel[numVertices];
	double totVertexVolume[numVertices];
	grads = (Vec3d**) malloc(sizeof(Vec3d*) * 4);
	Vec3d tmpVtx[4];
	double isotropicPressure;
	double elementVolume;
	for (int i = 0; i < numElements; i++) //element loop
	{
		for (int j = 0; j < 4; j++)
		{
			tmpVtx[j] =
					*verticesInitialPositions[tetMesh->getVertexIndex(i, j)];
		}
		initGrads = tetMesh->computeGradL(i, &tmpVtx[0], &tmpVtx[1], &tmpVtx[2],
				&tmpVtx[3]);
		elementVolume = tetMesh->getTetVolume(tetMesh->getVertex(i, 0),
				tetMesh->getVertex(i, 1), tetMesh->getVertex(i, 2),
				tetMesh->getVertex(i, 3));
		grads = tetMesh->computeGradL(i, tetMesh->getVertex(i, 0),
				tetMesh->getVertex(i, 1), tetMesh->getVertex(i, 2),
				tetMesh->getVertex(i, 3));
		//calculate spring-force and pressure-force
		Vec3d latticeSpringForce;
		Vec3d pressureTerm;
		double phi = phi0 * elementInitialVolume[i] / elementVolume;
		isotropicPressure = getFloryHuggins(phi) + 0.5 * c0 * phi / phi0;
		//cout << "isotropicPresure " << isotropicPressure << endl;
		double mobility = lambda0 * (1.0 - phi) * pow((phi0 / phi), 0.5);
		for (int j = 0; j < 4; j++) //vertex in element loop
		{
			Vec3d * vtxInitGrads = initGrads[j];
			Vec3d * gradLNew = grads[j];
			//printf("grads %d : %.1f   %.1f   %.1f\n", j, vtxGrads[0][0] , vtxGrads[0][1], vtxGrads[0][2]);
			latticeSpringForce = 0.0;
			pressureTerm = isotropicPressure * Vec3d(gradLNew[0]);
			for (int k = 0; k < 4; k++)
			{
				if (k == j)
					continue;
				Vec3d * vtxInitGrads2 = initGrads[k];
				latticeSpringForce += dot(Vec3d(vtxInitGrads[0]),
						Vec3d(vtxInitGrads2[0])) * (tmpVtx[k] - tmpVtx[j]);
//    		 printf("force: %d  %d\n", j, k);
				//std::cout << latticeSpringForce << std::endl;
				qVel[tetMesh->getVertexIndex(i, j)] +=
						(elementVolume * mobility)
								* (latticeSpringForce + pressureTerm);
				totVertexVolume[tetMesh->getVertexIndex(i, j)] += elementVolume;
			}
			//sum[i][j]=latticeSpringForce + pressureTerm;
			printf("force on vertex %d of element %d \n", j, i);
			std::cout << qVel[0] / totVertexVolume[0] << std::endl;
		}
		initGrads = NULL;
	}
	printf("\n********* Test run completed ******* \n");
	return 0;
}

int mainOld(int argc, char** argv)
{
	int numTimeSteps =10000;
	double deltaT = 0.0001;
	double temperature = 15.0;
	const double phi0 = 0.128571; //volume fraction of polymer in undeformed sample
	const double c0 = 0.00054;
	const double lambda0 = 1.0;

//	if (argc < 2)
//	{
//		std::cout << "InputFileName: " << argv[0] << std::endl;
//		std::cout << "loads and prints the input mesh element" << std::endl;
//		return 1;
//	}
//	char * inputFileName = argv[1];
	/*
	 * Already provide the input file
	 */
	ofstream outputFile("/home/santidan/Desktop/volume.txt");
	//char inputFileName[96] = "inputs/cubeTetgen.veg";
	char inputFileName[96] = "inputs/singleTet1.veg";
	VolumetricMesh * volumetricMesh = NULL;
	volumetricMesh = VolumetricMeshLoader::load(inputFileName);
	TetMesh * tetMesh;
	if (volumetricMesh->getElementType() == VolumetricMesh::TET)
		tetMesh = (TetMesh*) volumetricMesh;
	else
	{
		printf("Error: not a tet mesh.\n");
		exit(1);
	}
	int numElements = tetMesh->getNumElements();
	int numVertices = tetMesh->getNumVertices();
	int numElementVertices = tetMesh->getNumElementVertices();
	Vec3d verticesInitial[numVertices];
	for (int i = 0; i < numVertices; ++i)
	{
		verticesInitial[i] = *(tetMesh->getVertex(i));
//		printf("vertex %d  %f %f %f\n", i, verticesInitial[i][0],
//				verticesInitial[i][1], verticesInitial[i][2]);
	}
	Vec3d vertices[numVertices];
	for (int i = 0; i < numVertices; ++i)
	{
		vertices[i] = verticesInitial[i];
	}
	printf("\n");
	double totVolume0 = 0.0;
//  find initial volume of every element
//	double elementInitialVolume[numElements];
//	for (int i = 0; i < numElements; ++i)
//	{
//		printf("Element: %d  VertexIndex: ", i);
//		elementInitialVolume[i] = tetMesh->getElementVolume(i);
//		printf("\nVolume of tet Element %d : %.15f \n", i,
//				elementInitialVolume[i]);
//		totVolume0 += elementInitialVolume[i];
//	}
//	printf("\n");

	/*
	 *calculate the gradients of old L1, L2, L3, L4
	 *calculate elemental volume fraction
	 *calculate Flory-Huggins contribution, isotropic presure
	 */

	Vec3d qVel[numVertices];
	double totVertexVolume[numVertices];
	Vec3d initialVtx[numElementVertices];
	Vec3d currentVtx[numElementVertices];
	double isotropicPressure;
	double elementVolume;
	/*
	 * time evolution of the gel
	 */
	double totVolume;
	double phi;
	double mobility;
	double elementVolume0;
    double avgPhi;
	Vec3d ** initGrads;
	initGrads = (Vec3d**) malloc(sizeof(Vec3d*) * numElementVertices);
	Vec3d ** grads;
	grads = (Vec3d**) malloc(sizeof(Vec3d*) * numElementVertices);
	Vec3d vtxInitGrads;
	Vec3d vtxInitGrads2;
	Vec3d gradLNew;
	//outputFile.open ("/home/santi/Desktop/volume.txt");
	double chiValue = chi0(temperature);
	cout << "chiValue: " << chiValue <<endl;
	for (int iTimestep = 0; iTimestep < numTimeSteps; ++iTimestep)
	{
		totVolume = 0.0;
		totVolume0 = 0.0;
		phi = 0.0;
		mobility = 0.0;
		avgPhi = 0.0;
		memset(totVertexVolume,0,sizeof(totVertexVolume));
		for (int iElement = 0; iElement < numElements; ++iElement) //element loop
		{
			for (int j = 0; j < numElementVertices; ++j)
			{
				int vIndex = tetMesh->getVertexIndex(iElement, j);
				initialVtx[j] = verticesInitial[vIndex];
				currentVtx[j] = vertices[vIndex];
			}
			elementVolume0 = tetMesh->getTetVolume(&initialVtx[0], &initialVtx[1],
					&initialVtx[2], &initialVtx[3]);
			elementVolume = tetMesh->getTetVolume(&currentVtx[0], &currentVtx[1],
					&currentVtx[2], &currentVtx[3]);
			//cout << "computeGradL Initial vertex"  << endl;
			initGrads = tetMesh->computeGradL(&initialVtx[0], &initialVtx[1],
					&initialVtx[2], &initialVtx[3]);
			//cout << "computeGradL New vertex"  << endl;
			grads = tetMesh->computeGradL(&currentVtx[0], &currentVtx[1], &currentVtx[2],
					&currentVtx[3]);
			//calculate spring-force and pressure-force
			Vec3d latticeSpringForce;
			Vec3d pressureTerm;
			phi = phi0 * elementVolume0 / elementVolume;
			avgPhi += phi;
			isotropicPressure = getFloryHuggins(phi, chiValue) + (0.5 * c0 * phi / phi0);
			//cout << "isotropicPresure " << isotropicPressure << endl;
			mobility = lambda0 * (1.0 - phi) * pow((phi0 / phi), 0.5);
			for (int j = 0; j < numElementVertices; ++j) //vertex in element loop
			{
				vtxInitGrads =*initGrads[j];
				gradLNew = *grads[j];
				//printf("grads %d : %.1f   %.1f   %.1f\n", j, vtxInitGrads[0][0] , vtxInitGrads[0][1], vtxInitGrads[0][2]);
				//printf("grads %d : %.5f   %.5f   %.5f\n", j, gradLNew[0][0] , gradLNew[0][1], gradLNew[0][2]);
				latticeSpringForce = 0.0;
				pressureTerm = isotropicPressure * gradLNew;
				for (int k = 0; k < numElementVertices; ++k)
				{
					if (k == j)	continue;
					vtxInitGrads2 = *initGrads[k];
					latticeSpringForce += dot(vtxInitGrads,vtxInitGrads2) * (currentVtx[k] - currentVtx[j]);
//    		 printf("force: %d  %d\n", j, k);
					//std::cout << latticeSpringForce << std::endl;
				}
				qVel[tetMesh->getVertexIndex(iElement, j)] += (elementVolume
						* mobility)
						* (-c0 * latticeSpringForce
								+ (phi0 / phi) * pressureTerm);
				totVertexVolume[tetMesh->getVertexIndex(iElement, j)] += elementVolume0;
				//sum[i][j]=latticeSpringForce + pressureTerm;
				//printf("force on vertex %d of element %d \n", j, iElement);
				//std::cout << /*qVel[0] /*/ totVertexVolume[0] << std::endl;
			}
			totVolume += elementVolume;
			totVolume0 +=elementVolume0;
			for (int j = 0; j < numElementVertices; ++j) //vertex in element loop
				{
				 delete initGrads[j];
				 delete grads[j];
				}
			 free(initGrads);
			 free(grads);
		}				//element loop
		for (int iVertices = 0; iVertices < numVertices; ++iVertices)
		{
			vertices[iVertices] += qVel[iVertices]/totVertexVolume[iVertices] * deltaT;
			//std::cout << *vertices[iVertices] << std::endl ;
		}
		printf("%d  %f  %f  %f  %f \n", iTimestep, totVolume0, totVolume,pow((totVolume / totVolume0), 1. / 3.),avgPhi/numElements);
		if (iTimestep % 1000 ==0)
			outputFile << iTimestep << "  " << pow((totVolume / totVolume0), 1. / 3.) << " " << avgPhi/numElements << endl;
	} //iteration loop
	outputFile.close();
	printf("\n********* Test run completed ******* \n");
	//delete[] vtxInitGrads;
	//delete[] gradLNew;
	free(verticesInitial);
	free(vertices);
	//delete verticesInitial;
	delete tetMesh;
	delete volumetricMesh;
	return 0;
} // end of main()

double getFloryHuggins(double x)
{
	double termFH = -(x + log(1.0 - x) + (0.338 + 0.518 * x) * x * x);
	return (termFH);
}

//double getMobility(double x)

