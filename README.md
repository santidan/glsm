# README #

This README documents whatever steps are necessary to get the application up and running.

### What is this repository for? ###

* Quick summary

gLSM code implementation for linear tetrahedral element

* Version

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Download entire project, import as an eclipse project. The latest build is done on eclipse Neon M6. 

* Configuration
* Dependencies

[SUNDIALS: SUite of Nonlinear and DIfferential/ALgebraic Equation Solvers](http://computation.llnl.gov/projects/sundials-suite-nonlinear-differential-algebraic-equation-solvers)

* How to run tests

  ./gLSM++ (Look at function main() in the code in src folder for input files)

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Santidan Biswas (santidanbiswas@gmail.com)

* Other community or team contact